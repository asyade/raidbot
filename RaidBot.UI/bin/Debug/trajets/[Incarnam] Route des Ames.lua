MIN_MONSTERS = 1    -- 1 monstres minimum
MAX_MONSTERS = 5    -- 5 monstres maximum

function customNpcToAstrub()
    npc:npc(2889,3)
	npc:reply(-1)
	npc:reply(-1)
	global:delay(5000)
end

function incarnam()
    map:door(347)
end

function move()
	return {
        {map="-3,-3", use=489318, path="right"},
		{map="-2,-3", path="right", fight=true },
		{map="-1,-3", path="left|right", fight=true },
		{map="0,-3", path="left|right", fight=true },
		{map="1,-3", path="left|right", fight=true },
		{map="2,-3", path="left|right", fight=true },
		{map="3,-3", path="left", fight=true },
	}
end

function lost()
    return {
        {map="-2, -3", checkpoint=true},
        {map="-1, -3", path="left"},
        {map="0, -3", path="left"},
        {map="1, -3", path="left"},
        {map="2, -3", path="left"},
        {map="3, -3", path="left"},
    }
end