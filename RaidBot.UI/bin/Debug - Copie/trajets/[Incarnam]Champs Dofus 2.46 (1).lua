-------------------------------------------------
-- Auteur : Genesis
-- Zone : Incarnam (Champs) for Dofus v2.46
-- Départ : N'importe quelle map du script
-------------------------------------------------

-------------------------------------------------
-- Modifié par BANANEMAAAAAAAAAAN, Lunaytik
-------------------------------------------------

function sleep (n)
    local sec = tonumber(os.clock() + n);
    while (os.clock() < sec) do
    end
end

function isInDialog()
  sleep(2)
end

-- AUTO SUPRESSION -- Changer 0 par 2478, 16503 ici pour supprimer les Amulettes Aventuriers (2478) et les Coiffes Antômes (16503) --
AUTO_DELETE = {16513, 367}

-- Pourcentage de Pods pour le retour à la banque --
MAX_PODS = 90
REGEN_END = 90

-- COMBATS --
MIN_MONSTERS = 1 -- Nombre de monstres mini
MAX_MONSTERS = 8 -- Nombre de monstres maxi

-- AFFICHAGE --
DISPLAY_FIGHT_COUNT = true -- Affiche le compteur de combats

-- Fonction Move --
function talkToNpc()
    npc:npc(4398, 3)
    if not isInDialog() then
    end
    npc:reply(-1)
    npc:reply(-1)
end
function move()
  return {
    { map="3, 0", path="left"},
    { map="2, 0", path="left"},
    { map="1, 0", path="left"},
    { map="0, 0", path="left"},
    { map="-1, 0", path="left"},
    { map="-2, 0", path="top"},
    { map="-2, -1", path="top"},
    { map="-2, -2", path="top"},
    { map = "192415750", path = "424" },
    { map = "191104002", path = "right" },
    { map = "5,-18", path = "top" },
    { map = "5,-19", path = "right" },
    { map = "191106048", door= "383" },
    { map = "192416776", door= "455" },
        
    { map = "4,-3", path = "left" },
    { map = "3,-3", path = "left" },
    { map = "3,-3", path = "left" },
    { map = "2,-3", path = "left" },
    { map = "1,-3", path = "left" },
    { map = "0,-3", path = "left" },
    { map = "-1,-3", path = "left" },
    { map = "-2,-3", path = "top" },
    { map = "-2,-4", path = "right|top", fight = true },
    { map = "-2,-5", path = "left|right|top|bottom", fight = true },
    { map = "-3,-5", path = "right|top", fight = true },
    { map = "-3,-6", path = "right|bottom", fight = true },
    { map = "-2,-6", path = "right|left|bottom", fight = true },
    { map = "-1,-6", path = "left|bottom", fight = true },
    { map = "-1,-5", path = "right|top|left|bottom", fight = false },
    { map = "-1,-4", path = "top|left", fight = true },
    { map = "0,-4", path = "top|left", fight = true },
    { map = "0,-5", path = "left|bottom", fight = true },
  }
end
function bank()
	return {
		{ map = "-2,-4", path = "right" },
		{ map = "-1,-4", path = "right" },
		{ map = "0,-4", path = "right"},
		{ map = "-3,-5", path = "right"},
		{ map = "-3,-6", path = "right"},
		{ map = "-2,-5", path = "bottom"},
		{ map = "-2,-6", path = "bottom"},
		{ map = "-1,-6", path = "bottom"},
		{ map = "-1,-5", path = "bottom"},
		{ map = "0,-5", path = "bottom"},
		{ map = "1,-4", path = "right"},
		{ map = "2,-4", path = "right"},
		{ map = "3,-4", path = "bottom"},
		{ map = "3,-3", path = "right"},
		{ map = "4,-3", custom = talkToNpc},
        { map = "6,-19", path = "left" },
        { map = "5,-19", path = "bottom" },
        { map = "5,-18", path = "left" },
        { map = "191104002", door = "288" },
        { map = "192415750", npcBank = true },

	}
end
